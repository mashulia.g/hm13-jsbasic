const img = document.querySelectorAll('.img-show');
const btnStop = document.querySelector('.stop');
const btnStart = document.querySelector('.start');
let curr = 0;
let imgSlideTime;

function startAutoSlider () {
    imgSlideTime = setInterval( function () {
        img[curr].classList = 'hide';
        curr = (curr + 1) % img.length;
        img[curr].className = 'img-show';
    }, 3000)
}
startAutoSlider();

btnStop.addEventListener('click', function () {
    clearInterval(imgSlideTime);
})

btnStart.addEventListener('click', function () {
    startAutoSlider();
})

